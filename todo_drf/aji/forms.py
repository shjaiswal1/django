from cProfile import label
from unicodedata import name
from django  import forms
from .models import details

class Employeedetails(forms.ModelForm):
    class Meta:
        model = details
        labels = {
            'employee_name': 'Name',
            'employee_age': 'Age',
            'employee_location': 'Location',
           
        }
        fields = ('id',
                  'employee_name',
                  'employee_age',
                  'employee_location'
                )

        widgets = { 
            'employee_name': forms.TextInput(attrs={'class': 'form-control',
                                                    'id':'nameid'}),
            'employee_age': forms.TextInput(attrs={'class': 'form-control'
                                                     , 'id':'ageid'}),
            'employee_location': forms.TextInput(attrs={'class': 'form-control'
                                                         , 'id':'locationid'}),
           
            }                                     
                       
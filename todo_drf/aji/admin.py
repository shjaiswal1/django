from django.contrib import admin
from .models import details

@admin.register(details)
class Employeedetail(admin.ModelAdmin):
    list_display = ('id','employee_name', 'employee_age', 'employee_location')

# Register your models here.

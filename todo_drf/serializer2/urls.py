from django.urls import path
from serializer2 import views
from serializer2.views import UserViewSet
from serializer2.views import UserViewSet1
from serializer2.views import AccountViewSet
from rest_framework.routers import DefaultRouter
from django.urls import path, include

router = DefaultRouter()
router.register(r'users', UserViewSet, basename='user')
router.register(r'users1', UserViewSet1, basename='user1')
router.register(r'users2',AccountViewSet, basename='user2')

urlpatterns = [
    path('class/', views.SnippetList.as_view()),
    path('class/<int:pk>/', views.SnippetDetail.as_view()),
    
    
    path('gen/', views.SnippetList.as_view()),
    path('gen/<int:pk>/', views.SnippetDetail.as_view()),
]
urlpatterns += router.urls
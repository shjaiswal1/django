from asyncio.windows_events import NULL
from tkinter import CASCADE
from django.db import models

class Author(models.Model):
	name=models.CharField(max_length = 30,default='author name')
	place=models.CharField(max_length = 30, default='author place')
	
	
	def __str__(self):
		return self.name

class Book(models.Model):
	name = models.CharField(max_length = 50)
	email = models.EmailField(blank = True)
	describe = models.TextField(default = 'description')
	author = models.ForeignKey("Author",on_delete=models.CASCADE)
	picture = models.ImageField()
 
 
	def __str__(self):
		return self.name


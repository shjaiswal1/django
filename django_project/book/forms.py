from dataclasses import field
from django import forms
from .models import Author, Book

#DataFlair
class BookCreate(forms.ModelForm):

	class Meta:
		model = Book
		fields = '__all__'

